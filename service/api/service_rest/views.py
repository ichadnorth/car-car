from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Technician, Appointment
from .encoders import TechnicianEncoder, AppointmentEncoder
import json


@require_http_methods(["GET", "POST"])
def technicians(request):
    if request.method == "GET":
        try:
            technicians = Technician.objects.all()
        except Technician.DoesNotExist:
            return JsonResponse({"message": "Technicians unavailable"}, status=400)
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
        )
    else:
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False,
            status=200,
        )


@require_http_methods(["DELETE"])
def delete_technician(employee_id):
    try:
        technician = Technician.objects.get(employee_id=employee_id)
        technician.delete()
        return JsonResponse({"message": "Deleted"}, status=200)
    except Technician.DoesNotExist:
        return JsonResponse({"message": "Does not exist"}, status=404)


@require_http_methods(["GET", "POST"])
def appointments(request):
    if request.method == "GET":
        try:
            appointments = Appointment.objects.all()
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Appointment unavailable"},
                status=400,
            )
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            technician = Technician.objects.get(employee_id=content["technician"])
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid technician id"},
                status=400,
            )
        try:
            appointment = Appointment.objects.create(**content)
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Unable to create Appointment"},
                status=400,
            )
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )


@require_http_methods(["DELETE"])
def delete_appointment(id):
    count, _ = Appointment.objects.filter(id=id).delete()
    if count < 1:
        return JsonResponse({"message": "Appointment not found"}, status=404)
    return JsonResponse({"deleted": count > 0}, status=200)


@require_http_methods(["PUT"])
def cancel_appointment(request, id):
    try:
        appointment = Appointment.objects.get(id=id)
        appointment.status = "canceled"
        appointment.save()
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )
    except Appointment.DoesNotExist:
        return JsonResponse({"message": "Appointment not found"}, status=404)


@require_http_methods(["PUT"])
def finish_appointment(request, id):
    try:
        appointment = Appointment.objects.get(id=id)
        appointment.status = "finished"
        appointment.save()
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )
    except Appointment.DoesNotExist:
        return JsonResponse({"message": "Appointment not found"}, status=404)
