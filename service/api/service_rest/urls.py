from django.urls import path
from .views import (
    technicians,
    delete_technician,
    appointments,
    delete_appointment,
    cancel_appointment,
    finish_appointment
)

urlpatterns = [
    path("technicians/", technicians, name="technicians"),
    path("technicians/<str:employee_id>/", delete_technician, name="delete_technician"),
    path("appointments/", appointments, name="appointments"),
    path("appointments/<int:id>/", delete_appointment, name="delete_appointment"),
    path("appointments/<int:id>/cancel", cancel_appointment, name="cancel_appointment"),
    path("appointments/<int:id>/finish/", finish_appointment, name="finish_appointment"),
]
