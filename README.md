# CarCar

Team:

* Chad - Automobile Service
* Gianni - Automobile Sales

## Design
Our application is designed with three microservices: Automobile Inventory, Automobile Service, and Autombile Sales.


## Service microservice

    Models: Appointment, Technician, AutomobileVo

    - Create a technician
    - List technicians
    - Create an appointment w/ a specific technician
    - List appointments w/ status and VIP customer feature
    - Service History

## Sales microservice

    Models: Salesperson, Customers, Sales, AutomobileVo

    -Create a Salesperson
    -List salespeople
    -Create a customer
    -List customers
    -Create a sale that filters sold vins, and updates automobiles to sold
    -List sales
    -List salesperson history
