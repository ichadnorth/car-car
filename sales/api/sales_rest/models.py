from django.db import models
from django.urls import reverse

class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    vin = models.CharField(max_length=200)

    def __str__(self):
        return self.vin

class Salesperson(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    employee_id = models.CharField(max_length=10, unique=True)

    def __str__(self):
        return self.first_name + " " + self.last_name

class Customer(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=14)

    def get_api_url(self):
        return reverse("show_customer", kwargs={"id": self.id})

class Sale(models.Model):
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="sales_rest",
        on_delete=models.CASCADE,
    )
    salesperson = models.ForeignKey(
        Salesperson,
        related_name="sales_rest",
        on_delete=models.CASCADE,
    )
    customer = models.ForeignKey(
        Customer,
        related_name="sales_rest",
        on_delete=models.CASCADE,
    )
    price = models.IntegerField()
