import React, {useState, useEffect} from 'react'

function AutomobileForm () {
    const [model, setModel] = useState([])
    const [formData, setFormData] = useState({
        color: '',
        year: '',
        vin: '',
        model_id: '',
    })

    const getDataModel = async () => {
        const url = 'http://localhost:8100/api/models/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setModel(data.models);
        }
    }

    useEffect(() => {
        getDataModel();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();

        const automobilesUrl = 'http://localhost:8100/api/automobiles/';

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(automobilesUrl, fetchConfig);

        if (response.ok) {
            setFormData({
                color: '',
                year: '',
                vin: '',
                model_id: ''
            });
        }
    }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...formData,
            [inputName]: value
        });
    }

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add an automobile to inventory</h1>
            <form onSubmit={handleSubmit} id="create-automobile-form">
              <div className="form-floating mb-3">
                <input value={formData.color} onChange={handleFormChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input value={formData.year} onChange={handleFormChange} placeholder="year" required type="number" name="year" id="year" className="form-control" />
                <label htmlFor="year">Year</label>
              </div>
              <div className="form-floating mb-3">
                <input value={formData.vin} onChange={handleFormChange} placeholder="vin" required type="text" name="vin" id="vin" className="form-control" />
                <label htmlFor="vin">VIN</label>
              </div>
              <div className="mb-3">
                <select value={formData.model_id} onChange={handleFormChange} required name="model_id" id="model_id" className="form-select" >
                    <option value="">Choose a model</option>
                    {model.map(model => {
                        return (
                            <option key={model.id} value={model.id}>{model.name}</option>
                        )
                    })}
                </select>
                </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
}

export default AutomobileForm;
