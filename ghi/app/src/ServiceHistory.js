import React, { useState, useEffect } from 'react';

function ServiceHistory() {
  const [appointments, setAppointments] = useState([]);
  const [filteredAppointments, setFilteredAppointments] = useState([]);
  const [vin, setVin] = useState('');

  const [automobiles, setAutomobiles] = useState([]);

  const getAppointments = async () => {
    const response = await fetch('http://localhost:8080/api/appointments/');
    if (response.ok) {
      const data = await response.json();
      setAppointments(data.appointments);
      setFilteredAppointments(data.appointments);
    }
  };

  useEffect(() => {
    getAppointments();
    const fetchAutomobiles = async () => {
      const response = await fetch('http://localhost:8100/api/automobiles/');
      const data = await response.json();
      setAutomobiles(data.autos);
    };
    fetchAutomobiles();
  }, []);

  const handleSearch = (e) => {
    e.preventDefault();
    setFilteredAppointments(appointments.filter(app => app.vin === vin));
  };

  const isVip = (vin) => {
    const vins = automobiles.map((auto) => auto.vin)
    return vins.includes(vin)
  }


  return (
    <div>
      <h1>Service History</h1>
      <form onSubmit={handleSearch}>
        <input
          type="text"
          placeholder="Enter VIN"
          value={vin}
          onChange={(e) => setVin(e.target.value)}
        />
        <button type="submit">Search</button>
      </form>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>is VIP?</th>
            <th>Customer Name</th>
            <th>Date</th>
            <th>Time</th>
            <th>Technician Name</th>
            <th>Reason</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
          {filteredAppointments.map((appointment) => (
            <tr key={appointment.id}>
              <td>{appointment.vin}</td>
              <td>{isVip(appointment.vin) ? 'Yes' : 'No'}</td>
              <td>{appointment.customer}</td>
              <td>{new Date(appointment.date_time).toLocaleDateString()}</td>
              <td>{new Date(appointment.date_time).toLocaleTimeString()}</td>
              <td>
                {appointment.technician.first_name}{' '}
                {appointment.technician.last_name}
              </td>
              <td>{appointment.reason}</td>
              <td>{appointment.status}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default ServiceHistory;
