import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import SalespeopleList from './SalespeopleList';
import CustomersList from './CustomersList';
import SalesList from './SalesList';
import SalespersonForm from './SalespersonForm';
import CustomerForm from './CustomerForm';
import AppointmentForm from './AppointmentForm';
import TechnicianForm from './TechnicianForm';
import TechnicianList from './TechnicianList';
import AppointmentList from './AppointmentList';
import ServiceHistory from './ServiceHistory';
import SaleForm from './SalesForm';
import ManufacturersList from './ManufacturersList';
import ModelsList from './ModelsList';
import AutomobilesList from './AutomobilesList';
import ManufacturerForm from './ManufacturerForm';
import AutomobileForm from './AutomobilesForm';
import ModelForm from './ModelForm';
import SalesHistoryList from './SalespersonHistory';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="salespeople" element={<SalespeopleList />} />
          <Route path="salespeople/new" element={<SalespersonForm />} />
          <Route path="customers" element={<CustomersList />} />
          <Route path="customers/new" element={<CustomerForm />} />
          <Route path="sales" element={<SalesList />} />
          <Route path="appointments/new" element={<AppointmentForm />} />
          <Route path="appointments" element={<AppointmentList />} />
          <Route path="technicians/new" element={<TechnicianForm />} />
          <Route path="technicians" element={<TechnicianList />} />
          <Route path="servicehistory" element={<ServiceHistory />} />

          <Route path="sales/new" element={<SaleForm />} />
          <Route path="manufacturers" element={<ManufacturersList />} />
          <Route path="manufacturers/new" element={<ManufacturerForm />} />
          <Route path="models" element={<ModelsList />} />
          <Route path="models/new" element={<ModelForm />} />
          <Route path="automobiles" element={<AutomobilesList />} />
          <Route path="automobiles/new" element={<AutomobileForm />} />
          <Route path="saleshistory" element={<SalesHistoryList />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
