import React, { useState, useEffect } from 'react';

function AppointmentForm() {

  const [formData, setFormData] = useState({
    vin: '',
    customer: '',
    date: '',
    time: '',
    technician: '',
    reason: '',
  });

  const handleSubmit = async (e) => {
    e.preventDefault();

    const appointmentUrl = 'http://localhost:8080/api/appointments/';

    const { date, time, ...rest } = formData;

    const updatedformData = {
      ...rest,
      date_time: `${formData.date}T${formData.time}`,
    };

    const fetchConfig = {
      method: 'post',
      body: JSON.stringify(updatedformData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(appointmentUrl, fetchConfig);

    if (response.ok) {
      setFormData({
        vin: '',
        customer: '',
        date: '',
        time: '',
        technician: '',
        reason: '',
      });
    }
  };

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;
    setFormData({
      ...formData,
      [inputName]: value,
    });
  };

  const [technicians, setTechnicians] = useState([]);

  useEffect(() => {
    const fetchTechnicians = async () => {
      const response = await fetch('http://localhost:8080/api/technicians/');
      const data = await response.json();
      setTechnicians(data.technicians);
    };
    fetchTechnicians();
  }, []);


  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a Service Appointment</h1>
          <form onSubmit={handleSubmit} id="create-appointment-form">
            <div className="form-floating mb-3">
              <input
                value={formData.vin}
                onChange={handleFormChange}
                placeholder="VIN"
                required
                type="text"
                name="vin"
                id="vin"
                className="form-control"
              />
              <label htmlFor="vin">VIN</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={formData.customer}
                onChange={handleFormChange}
                placeholder="Customer Name"
                required
                type="text"
                name="customer"
                id="customer"
                className="form-control"
              />
              <label htmlFor="customer">Customer Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={formData.date}
                onChange={handleFormChange}
                placeholder="Appointment Date"
                required
                type="date"
                name="date"
                id="date"
                className="form-control"
              />
              <label htmlFor="date">Appointment Date</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={formData.time}
                onChange={handleFormChange}
                placeholder="Appointment Time"
                required
                type="time"
                name="time"
                id="time"
                className="form-control"
              />
              <label htmlFor="time">Appointment Time</label>
            </div>
            <div className="form-floating mb-3">
              <select
                value={formData.technician}
                onChange={handleFormChange}
                required
                name="technician"
                id="technician"
                className="form-control"
              >
                <option value="">Select Technician</option>
                {technicians.map((tech) => (
                  <option key={tech.employee_id} value={tech.employee_id}>
                    {tech.first_name} {tech.last_name}
                  </option>
                ))}
              </select>
              <label htmlFor="technician">Technician</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={formData.reason}
                onChange={handleFormChange}
                placeholder="Reason"
                required
                type="text"
                name="reason"
                id="reason"
                className="form-control"
              />
              <label htmlFor="reason">Reason</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default AppointmentForm;
