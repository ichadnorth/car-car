import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

function AppointmentList() {
  const [appointments, setAppointments] = useState([]);

  const [automobiles, setAutomobiles] = useState([]);

  useEffect(() => {
    const fetchAppointments = async () => {
      const response = await fetch('http://localhost:8080/api/appointments/');
      const data = await response.json();
      setAppointments(data.appointments);
    };
    fetchAppointments();

    const fetchAutomobiles = async () => {
      const response = await fetch('http://localhost:8100/api/automobiles/');
      const data = await response.json();
      setAutomobiles(data.autos);
    };
    fetchAutomobiles();
  }, []);

  const handleCancelAppointment = async (id) => {
    const response = await fetch(`http://localhost:8080/api/appointments/${id}/cancel`, { method: 'PUT' });
    if (response.ok) {
      setAppointments(appointments.filter((appointment) => appointment.id !== id));
    }
  };

  const handleFinishAppointment = async (id) => {
    const response = await fetch(`http://localhost:8080/api/appointments/${id}/finish/`, { method: 'PUT' });
    if (response.ok) {
      setAppointments(appointments.filter((appointment) => appointment.id !== id));
    }
  };

  const isVip = (vin) => {
    const vins = automobiles.map((auto) => auto.vin)
    return vins.includes(vin)
  }

  return (
    <div>
      <h1>Service Appointments</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Is VIP?</th>
            <th>Customer</th>
            <th>Date</th>
            <th>Time</th>
            <th>Technician</th>
            <th>Reason</th>
          </tr>
        </thead>
        <tbody>
          {appointments
            .filter((appointment) => appointment.status === 'created')
            .map((appointment) => (
              <tr key={appointment.id}>
                <td>{appointment.vin}</td>
                <td>{isVip(appointment.vin) ? 'Yes' : 'No'}</td>
                <td>{appointment.customer}</td>
                <td>{new Date(appointment.date_time).toLocaleDateString()}</td>
                <td>{new Date(appointment.date_time).toLocaleTimeString()}</td>
                <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                <td>{appointment.reason}</td>
                <td>
                  {appointment.status !== 'cancel' ? (
                    <>
                      <button
                        className="btn btn-danger"
                        onClick={() => handleCancelAppointment(appointment.id)}
                      >
                        Cancel
                      </button>
                      <button
                        className="btn btn-success ms-2"
                        onClick={() => handleFinishAppointment(appointment.id)}
                      >
                        Finish
                      </button>
                    </>
                  ) : null}
                </td>
              </tr>
            ))}
        </tbody>
      </table>
    </div>
  );
}

export default AppointmentList;
