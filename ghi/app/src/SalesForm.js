import React, {useState, useEffect} from 'react';

function SaleForm () {
    const [autos, setAutos] = useState([])
    const [salesperson, setSalesperson] = useState([])
    const [customer, setCustomer] = useState([])
    const [formData, setFormData] = useState({
        automobile: '',
        salesperson: '',
        customer: '',
        price: '',
    })

    const getDataAutos = async () => {
        const url = 'http://localhost:8100/api/automobiles/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setAutos(data.autos);
        }
    }

    const getDataSalesperson = async () => {
        const url = 'http://localhost:8090/api/salespeople/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setSalesperson(data.salespeople);
        }
    }

    const getDataCustomer = async () => {
        const url = 'http://localhost:8090/api/customers/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setCustomer(data.customers);
        }
    }

    useEffect(() => {
        getDataAutos();
        getDataSalesperson();
        getDataCustomer();
    }, []);

    const updateAutomobileStatus = async (vin) => {
        const url = `http://localhost:8100/api/automobiles/${vin}/`;
        const fetchConfig = {
          method: "put",
          body: JSON.stringify({ sold: true }),
          headers: {
            "Content-Type": "application/json",
          },
        };

        const response = await fetch(url, fetchConfig);
            if (response.ok) {
            getDataAutos();
            }
        };


    const handleSubmit = async (event) => {
        event.preventDefault();

        const salesUrl = 'http://localhost:8090/api/sales/';

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(salesUrl, fetchConfig);

        if (response.ok) {
            updateAutomobileStatus(formData.automobile);
            setFormData({
                automobile: '',
                salesperson: '',
                customer: '',
                price: '',
            });
        }
    }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...formData,
            [inputName]: value
        });
    }

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add Sale</h1>
            <form onSubmit={handleSubmit} id="create-sales-form">
              <div className="mb-3">
                <select value={formData.automobile} onChange={handleFormChange} required name="automobile" id="automobile" className="form-select" >
                    <option value="">Choose an automobile VIN</option>
                    {autos
                    .filter((automobile) => automobile.sold == false)
                    .map(automobile => {
                        return (
                            <option key={automobile.vin} value={automobile.vin}>{automobile.vin}</option>
                        )
                    })}
                </select>
                </div>
                <div className="mb-3">
                <select value={formData.salesperson} onChange={handleFormChange} required name="salesperson" id="salesperson" className="form-select" >
                    <option value="">Choose an salesperson</option>
                    {salesperson.map(salesperson => {
                        return (
                            <option key={salesperson.employee_id} value={salesperson.employee_id}>{salesperson.first_name} {salesperson.last_name}</option>
                        )
                    })}
                </select>
                </div>
                <div className="mb-3">
                <select value={formData.customer} onChange={handleFormChange} required name="customer" id="customer" className="form-select" >
                    <option value="">Choose a customer</option>
                    {customer.map(customer => {
                        return (
                            <option key={customer.id} value={customer.id}>{customer.first_name} {customer.last_name}</option>
                        )
                    })}
                </select>
                </div>
                <div className="form-floating mb-3">
                <input value={formData.price} onChange={handleFormChange} placeholder="Price" required type="number" name="price" id="price" className="form-control" />
                <label htmlFor="price">Price</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
}

export default SaleForm;
