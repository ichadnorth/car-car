import { useEffect, useState } from 'react';

function SalesHistoryList() {
    const [sales, setSales] = useState([])
    const [salesperson, setSalesperson] = useState([])
    const [filterValue, setFilterValue] = useState("")

    const getData = async () => {
        const response = await fetch('http://localhost:8090/api/sales/');

        if (response.ok) {
            const data = await response.json();
            setSales(data.sales)
        }
    }
    const getDataSalesperson = async () => {
        const url = 'http://localhost:8090/api/salespeople/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setSalesperson(data.salespeople);
        }
    }

    useEffect(()=>{
        getData();
        getDataSalesperson();
    }, [])

    const handleFilterValueChange = (e) => {
        const value = e.target.value
        setFilterValue(value)
    }

    return (
        <div>
            <h1>Sales</h1>
            <select onChange={handleFilterValueChange} value={filterValue} placeholder='Salesperson'>
                <option value="">Select Salesperson</option>
                {salesperson.map(salesperson => (
                    <option key={salesperson.employee_id} value={salesperson.employee_id}>
                        {salesperson.first_name} {salesperson.last_name}
                    </option>
                ))}
            </select>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Salesperson Name</th>
                        <th>Customer</th>
                        <th>VIN</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {sales
                    .filter(sale => filterValue === "" || sale.salesperson.employee_id === filterValue)
                    .map(sale => {
                        return (
                            <tr key={sale.id}>
                                <td>{ sale.salesperson.first_name } { sale.salesperson.last_name }</td>
                                <td>{ sale.customer.first_name } { sale.customer.last_name }</td>
                                <td>{ sale.automobile.vin }</td>
                                <td>${ sale.price }</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}

export default SalesHistoryList;
