import React, { useState, useEffect } from 'react';

function TechnicianList() {
  const TECHNICIANS_LIST_URL = 'http://localhost:8080/api/technicians/';
  const [technicians, setTechnicians] = useState([]);

  const getData = async () => {
    const response = await fetch(TECHNICIANS_LIST_URL);
    if (response.ok) {
      const data = await response.json();
      setTechnicians(data.technicians);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <div className="px-4 py-5 my-5">
      <h1 className="display-5 fw-bold text-left">Technicians</h1>
      <div className="col-lg-10 mx-auto">
        <table className="table table-striped table-hover mt-4">
          <thead>
            <tr>
              <th>Employee ID</th>
              <th>First Name</th>
              <th>Last Name</th>
            </tr>
          </thead>
          <tbody>
            {technicians.map((technician) => {
              return (
                <tr key={technician.employee_id}>
                  <td>{technician.employee_id}</td>
                  <td>{technician.first_name}</td>
                  <td>{technician.last_name}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </div>
  );
}

export default TechnicianList;
